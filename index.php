<?php
    include 'mini_chat.php';
    
    if(isset($_POST['submit']) || isset($_POST['actualiser']))
    {
        $pseudo = $_POST['pseudo'];
        $message = $_POST['message'];
        
        if($pseudo && $message && isset($_POST['submit']))
        {
            if(isset($_COOKIE['pseudo']) && $_COOKIE['pseudo'] == $pseudo)
            {
                inserMessage($pseudo, $message);
            }
            
            else {
                setcookie('pseudo', $pseudo, time() + 600, null, null, false, true);
                inserMessage($pseudo, $message);
            }
        }
        
        else 
        {
            header('Location:index.php');
        }
    }
    
    ?>

<!DOCTYPE html>

<html lang="fr" >
    <head>
        <meta charset="UTF-8" />
        <title>Mini chat <?= get_version(); ?></title>
    </head>
    
    <body>
        <header>
            <?php
                echo "<h1>Bienvenue sur le chat.</h1>";
                salut();
            ?>
        </header>
        
        <form method="post" action="index.php">
            <label for="pseudo">Pseudo :</label>
            <?php pseudo(); ?>
            
            <label for="message">Message :</label>
            <input type="text" id="message" name="message" placeholder='Ex: Message' />
            
            <input type="submit" value="Envoyer" name="submit" />
            <input type="submit" value="Actualiser" name="actualiser" />
        </form>
        
        <p>Le nombre de lingne dans la table : <?= get_nbMessage()?></p>
        
        <hr />
        
        <p>
            <?php afficheMessage(); ?>
        </p>
        
        <hr />
        
        <footer>
            <p>
                <a href="showMessage.php?onglet=1"><input type="submit" value="Tout les messages" /></a>
            </p>
        </footer>
    </body>
</html>