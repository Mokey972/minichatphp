<?php
    include 'mini_chat.php';
    
    if(isset($_POST['show']) || isset($_POST['noshow']) || isset($_COOKIE['show']))
    {
        setcookie('show', false, time() + 20, null, null, false, true);
        if(isset($_POST['show']) || isset($_COOKIE['show']))
        {
            $_COOKIE['show'] = true;
        }
        elseif (isset ($_POST['noshow']) || isset($_COOKIE['show']))
        {
            $_COOKIE['show'] = false;
            header('Location:showMessage.php?onglet=1');
        }
    }
?>

<!DOCTYPE html>

<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Tout le chat <?= get_version();?></title>
    </head>
    <body>
        <p>
            <a href="index.php"><input type="submit" value="retour."/></a> <br />
        </p>
        
        <form method="post" action="showMessage.php">
            <?php
                if(isset($_COOKIE['show']) and $_COOKIE['show'])
                {
                    ?>
                    <input type="submit" value="Masquer" name="noshow" />
                    <br />
                    <?php
                    fullShowMessage(); 
                }

                else {?>
                    <input type="submit" value="Affiche tous les messages" name="show" />
                <?php
                    }
            ?>
        </form>
        
        <form method="get" action="showMessage.php">
            
            <p>
            <?php 
                if(isset($_GET['onglet']))
                {
                    afficheOnglet($_GET['onglet']);
                    showOnglet();
                }
            ?>
            </p>
        </form>
    </body>
</html>
