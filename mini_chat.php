<?php

                    /** Creation de la base de donnée **/

function myData()
{
    $server = 'localhost';
    $login = 'omvadmin';
    $password = 'linux';
    
    try
    {
        $dateBase = new PDO("mysql:host=$server; dbname=mini_chat", $login, $password) or die('Error : data base');
        $dateBase->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    } catch (PDOException $ex) {
        echo "<p>Erreur : $ex->getMessage() </p>";
    }
    
    return $dateBase;
}

                    /** Les fonctoin **/

function salut()
{
    echo "<p>Hello world</p>";
}

function inserMessage($pseudo, $message)
{
    $bdd = myData();
    
    $query = $bdd->prepare("INSERT INTO minichat(pseudo, message, date_creation)"
                    . "VALUES(:pseudo, :message, now())");
    
    $query->execute(array(
                'pseudo' => $pseudo,
                'message' => $message
            ));
    
    $query->closeCursor();
    
    header('Location:index.php');
}

function afficheOnglet($nbOnglet)
{   
    if(intval($nbOnglet) > 0 and intval($nbOnglet) <= get_nbVolet())
    {
        $bdd = myData();
     
        $min =  ($nbOnglet * 15) - 15;

        $reponse = $bdd->query('SELECT * FROM minichat ORDER BY id DESC LIMIT '.$min.', 15');

        while($donnees = $reponse->fetch())
        {
            echo '<strong>' .htmlspecialchars($donnees['pseudo']). ': </strong> ' .htmlspecialchars($donnees['message'])
                    .' <em>Date : ' . htmlspecialchars($donnees['date_creation']).'</em><br />';
        }

        $reponse->closeCursor();
    }
    
    else
    {
        echo '<h1><em>404</em> Page not found</h1>';
    }

}

function afficheMessage()
{
    $bdd = myData();
    
    $reponse = $bdd->query('SELECT * FROM minichat ORDER BY id DESC LIMIT 0, 10');
    
    while($donnees = $reponse->fetch())
    {
        echo '<strong>' .htmlspecialchars($donnees['pseudo']). ': </strong> ' .htmlspecialchars($donnees['message'])
                . ' <em>Date : ' . htmlspecialchars($donnees['date_creation']).'</em><br />';
    }
    
    $reponse->closeCursor();
}

function fullShowMessage()
{
    $bdd = myData();
    
    $reponse = $bdd->query('SELECT * FROM minichat');
    
    while($donnees = $reponse->fetch())
    {
        echo '<strong>' .htmlspecialchars($donnees['pseudo']). ': </strong> ' .htmlspecialchars($donnees['message'])
                . ' <em>Date : ' . htmlspecialchars($donnees['date_creation']).'</em><br />';
    }
    
    $reponse->closeCursor();
}

function pseudo()
{
    if(isset($_COOKIE['pseudo']))
    {
        echo '<input type="text" id="pseudo" name="pseudo" value="'. htmlspecialchars($_COOKIE["pseudo"]).'" />';
    }
    
    else
    {
        echo "<input type='text' id='pseudo' name='pseudo' placeholder='Ex: Pseudo' />";
    }
}

function showOnglet()
{
    for($i = 1; $i <= get_nbVolet(); $i++)
    {
        echo '<a href="showMessage.php?onglet='.$i.'"> '.$i.' </a>';
    }
}


                    /** Les getting ***/

function get_pseudo()
{
    $bdd = myData();
    
    $reponse = $bdd->query('SELECT pseudo FROM minichat ORDER BY id DESC');
    
    $donnee = $reponse->fetch();
    
    $reponse->closeCursor();
    
    return $donnee['pseudo'];
}

function get_nbMessage()
{
    $bdd = myData();
    
    $nbMessage = $bdd->query('SELECT COUNT(*) AS nbMessage FROM minichat');
    
    $nombre = $nbMessage->fetch();
    
    $nbMessage->closeCursor();
    
    return $nombre['nbMessage'];
}

function get_nbVolet()
{
    $onglet = get_nbMessage() / 15;
    
    if(is_int($onglet))
    {
        return $onglet;
    }
    
    else
    {
        $onglet = intval($onglet) + 1;
        return $onglet;
    }
}

function get_version()
{
    $version = '1.9-dev';
    
    return $version;
}